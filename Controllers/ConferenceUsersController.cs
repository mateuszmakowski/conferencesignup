﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ConfereceSignUp.Models;

namespace ConfereceSignUp.Controllers
{
    public class ConferenceUsersController : Controller
    {
        private readonly ConfereceSignUpContext _context;

        public ConferenceUsersController(ConfereceSignUpContext context)
        {
            _context = context;
        }

        // GET: ConferenceUsers
        public async Task<IActionResult> Index()
        {
            return View(await _context.ConferenceUser.ToListAsync());
        }

        // GET: ConferenceUsers/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var conferenceUser = await _context.ConferenceUser
                .FirstOrDefaultAsync(m => m.Id == id);
            if (conferenceUser == null)
            {
                return NotFound();
            }

            return View(conferenceUser);
        }

        // GET: ConferenceUsers/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: ConferenceUsers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,FirstName,LastName,Email,ConferenceType")] ConferenceUser conferenceUser)
        {
            if (ModelState.IsValid)
            {
                _context.Add(conferenceUser);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(conferenceUser);
        }

        // GET: ConferenceUsers/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var conferenceUser = await _context.ConferenceUser.FindAsync(id);
            if (conferenceUser == null)
            {
                return NotFound();
            }
            return View(conferenceUser);
        }

        // POST: ConferenceUsers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,FirstName,LastName,Email,ConferenceType")] ConferenceUser conferenceUser)
        {
            if (id != conferenceUser.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(conferenceUser);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ConferenceUserExists(conferenceUser.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(conferenceUser);
        }

        // GET: ConferenceUsers/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var conferenceUser = await _context.ConferenceUser
                .FirstOrDefaultAsync(m => m.Id == id);
            if (conferenceUser == null)
            {
                return NotFound();
            }

            return View(conferenceUser);
        }

        // POST: ConferenceUsers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var conferenceUser = await _context.ConferenceUser.FindAsync(id);
            _context.ConferenceUser.Remove(conferenceUser);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ConferenceUserExists(int id)
        {
            return _context.ConferenceUser.Any(e => e.Id == id);
        }
    }
}
