﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace ConfereceSignUp.Models
{
    public class ConfereceSignUpContext : DbContext
    {
        public ConfereceSignUpContext (DbContextOptions<ConfereceSignUpContext> options)
            : base(options)
        {
        }

        public DbSet<ConfereceSignUp.Models.ConferenceUser> ConferenceUser { get; set; }
    }
}
