﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ConfereceSignUp.Models
{
    public enum ConferenceType
    {
        [Display(Name = "Wykład")]
        Lecture,
        [Display(Name = "Warsztaty")]
        Workshop,
        [Display(Name = "Dyskusja")]
        Discussion
    }

    public class ConferenceUser
    {
        public int Id { get; set; }
        //public string Avatar { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public ConferenceType? ConferenceType { get; set; }
    }
}
